package nna.vozforums.supervisor.injection.component;

import dagger.Component;
import nna.vozforums.supervisor.ui.box.BoxFragment;
import nna.vozforums.supervisor.ui.login.LoginFragment;
import nna.vozforums.supervisor.ui.main.MainActivity;
import nna.vozforums.supervisor.injection.PerActivity;
import nna.vozforums.supervisor.injection.module.ActivityModule;
import nna.vozforums.supervisor.ui.post.PostFragment;
import nna.vozforums.supervisor.ui.reply.ReplyFragment;
import nna.vozforums.supervisor.ui.topic.TopicFragment;
import nna.vozforums.supervisor.ui.usercp.UserCpFragment;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity mainActivity);

    void inject(BoxFragment boxFragment);

    void inject(TopicFragment topicFragment);

    void inject(PostFragment postFragment);

    void inject(LoginFragment loginFragment);

    void inject(ReplyFragment replyFragment);

    void inject(UserCpFragment userCpFragment);

}

