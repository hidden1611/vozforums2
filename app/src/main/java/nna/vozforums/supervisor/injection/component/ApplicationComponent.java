package nna.vozforums.supervisor.injection.component;

import android.app.Application;
import android.content.Context;

import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Component;
import nna.vozforums.supervisor.VozApplication;
import nna.vozforums.supervisor.data.DataManager;
import nna.vozforums.supervisor.data.model.User;
import nna.vozforums.supervisor.injection.ApplicationContext;
import nna.vozforums.supervisor.injection.module.ApplicationModule;
import nna.vozforums.supervisor.ui.box.BoxPresenter;
import nna.vozforums.supervisor.ui.main.MainPresenter;
import nna.vozforums.supervisor.ui.post.PostPresenter;
import nna.vozforums.supervisor.ui.topic.TopicPresenter;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(VozApplication ikyBikeApplication);

    @ApplicationContext
    Context context();
    Application application();
    Bus eventBus();
    DataManager dataManager();
    User user();


    void inject(MainPresenter mainPresenter);

    void inject(BoxPresenter boxPresenter);

    void inject(TopicPresenter topicPresenter);

    void inject(PostPresenter postPresenter);
}
