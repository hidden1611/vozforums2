package nna.vozforums.supervisor.data.model;

import android.text.TextUtils;

import org.jsoup.nodes.Element;

import nna.vozforums.supervisor.data.remote.VozService;

/**
 * Created by Ann on 3/13/16.
 */
public class Post {

    private boolean isOnline;

    String user;
    String urlAvatar;
    String idUser;
    String idPost;
    String title;
    String jd;
    String posts;
    String numberPost;
    String timePost;
    String htmlContent;

    public Post(Element element) throws Exception {
        /*
        <table class="tborder voz-postbit" id="post84857402" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
 <tbody>
  <tr>
   <td class="thead">
    <div class="normal" style="float:right">
      &nbsp; #
     <a href="showpost.php?p=84857402&amp;postcount=2" target="new" rel="nofollow" id="postcount84857402" name="2"><strong>2</strong></a> &nbsp;
    </div>
    <div class="normal">
     <!-- status icon and date -->
     <a name="post84857402"><img class="inlineimg" src="images/statusicon/post_old.gif" alt="Old" border="0"></a> 09-01-2016, 19:47
     <!-- / status icon and date -->
    </div> </td>
  </tr>
  <tr>
   <td class="alt2" style="padding:0px">
    <!-- user info -->
    <table cellpadding="0" cellspacing="6" border="0" width="100%">
     <tbody>
      <tr>
       <td class="alt2"><a href="member.php?u=1138453"><img src="customavatars/avatar1138453_7.gif" width="80" height="60" alt="Virgo199's Avatar" border="0"></a></td>
       <td nowrap>
        <div id="postmenu_84857402">
         <a class="bigusername" href="member.php?u=1138453">Virgo199</a>
         <!-- BEGIN TEMPLATE: postbit_onlinestatus -->
         <img class="inlineimg" src="images/statusicon/user_offline.gif" alt="Virgo199 is offline" border="0">
         <!-- END TEMPLATE: postbit_onlinestatus -->
         <script type="text/javascript"> vbmenu_register("postmenu_84857402", true); </script>
        </div>
        <div class="smallfont">
         Đã tốn tiền
        </div> </td>
       <td width="100%">&nbsp;</td>
       <td valign="top" nowrap>
        <div class="smallfont">
         <div>
          Join Date: 10-2012
         </div>
         <div class="voz-postbit-location voz-truncate">
          Location: From Heaven with shit
         </div>
         <div>
           Posts: 1,456
         </div>
         <div>
         </div>
        </div> </td>
      </tr>
     </tbody>
    </table>
    <!-- / user info --> </td>
  </tr>
  <tr>
   <td class="alt1" id="td_post_84857402">
    <!-- message, attachments, sig -->
    <!-- icon and title -->
    <div class="smallfont">
     <strong>Re: [CES 2016] Remix OS: Công cụ miễn phí giúp chạy Android trên máy tính</strong>
    </div>
    <hr size="1" style="color:#FFFFFF; background-color:#FFFFFF">
    <!-- / icon and title -->
    <!-- message -->
    <div id="post_message_84857402" class="voz-post-message">
     <a href="/redirect/index.php?link=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DmpYR0IQ7C8U" target="_blank">https://www.youtube.com/watch?v=mpYR0IQ7C8U</a>
     <br>
     <br> Remix OS 2 Demo
     <img src="/images/smilies/Off/smile.gif" border="0" alt="" title="Smile" class="inlineimg">
    </div>
    <!-- / message -->
    <div style="margin-top: 10px" align="right">
     <!-- controls -->
     <a href="newreply.php?do=newreply&amp;p=84857402" rel="nofollow"><img src="images/buttons/quote.gif" alt="Reply With Quote" border="0"></a>
     <!-- / controls -->
    </div>
    <!-- message, attachments, sig --> </td>
  </tr>
 </tbody>
</table>
         */
        Element eleUser = element.select("tr td[style]").first();
        user = eleUser.select("a[class=bigusername]").text();
        idUser = eleUser.select("a[class=bigusername]").attr("href").split("=")[1];
        urlAvatar = eleUser.select("img[src^=customavatars]").attr("src");
        if(urlAvatar != null && !TextUtils.isEmpty(urlAvatar)){
            urlAvatar = urlAvatar.startsWith("http") ? urlAvatar : VozService.ENDPOINT + urlAvatar;
        }else {
            urlAvatar = null;
        }
        title = eleUser.select("div[id^=postmenu]").first().nextElementSibling().text();
        jd = eleUser.select("div:containsOwn(Join Date)").text().replace("Join Date: ","");
        posts = eleUser.select("div:containsOwn(Posts: )").text().replace("Posts: ","");
        if(eleUser.select("img[src*=line.gif").first() != null){
            if(eleUser.select("img[src*=line.gif").attr("src").contains("online"))
                isOnline = true;
            else
                isOnline = false;
        }

        Element eleTime = element.select("tr").get(0);
        numberPost = "#" + eleTime.select("a[href^=showpost]").text();
        try {
            timePost = eleTime.select("div").get(1).text();
        } catch (Exception e) {
            timePost = "";
            e.printStackTrace();
        }

        Element eleMessage = element.select("div[id^=post_message]").first();
        htmlContent = eleMessage.html();
        idPost = eleMessage.attr("id").replace("post_message_","");
    }

    public boolean isOnline() {
        return isOnline;
    }

    public String getUser() {
        return user;
    }

    public String getUrlAvatar() {
        return urlAvatar;
    }

    public String getIdUser() {
        return idUser;
    }

    public String getIdPost() {
        return idPost;
    }

    public String getTitle() {
        return title;
    }

    public String getJd() {
        return jd;
    }

    public String getPosts() {
        return posts;
    }

    public String getNumberPost() {
        return numberPost;
    }

    public String getTimePost() {
        return timePost;
    }

    public String getHtmlContent() {
        return htmlContent;
    }
}
