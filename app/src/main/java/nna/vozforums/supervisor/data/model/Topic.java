package nna.vozforums.supervisor.data.model;

import org.jsoup.nodes.Element;

import nna.vozforums.supervisor.data.remote.VozService;

/**
 * Created by Ann on 3/2/16.
 */
public class Topic {
    String name;
    String title;
    String id;
    String link;
    String linkFirst;
    String linkLast;
    String linkNewPost;
    String imgStatusIcon;
    String user;
    String time;
    String replies;
    boolean isSticky;

    int totalPage;
    int currentPage;

    public Topic(){

    }

    public void setName(String name) {
        this.name = name;
    }


    public Topic(Element element) {

        name = element.select("a[id]").text();
        link = element.select("a[id]").attr("href");
        link = link.startsWith("http") ? link: VozService.ENDPOINT + link;
        id = link.split("t=")[1];

        if (element.select("a[id*=thread_gotonew]").first() != null) {
            linkNewPost = element.select("a[id*=thread_gotonew]").first().attr("href");
        }
        user = element.select("span[onclick]").text();
        title = element.select("td[id^=td_threadtitle]").attr("title");
        replies = element.select("td[class=alt2]").attr("title");
        time = element.select("div[style]").text();
        time = time.replace(" by ", " | ");

        isSticky = false;
        if(element.select("a[id*=thread_title") != null
                && element.select("a[id*=thread_title").hasClass("vozsticky")){
            isSticky = true;
        }

    }

    public boolean isSticky() {
        return isSticky;
    }

    public String getReplies() {
        return replies;
    }

    public String getTime() {
        return time;
    }


    public String getName() {
        return name;
    }

    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }

    public String getLink() {
        return link;
    }

    public String getLinkFirst() {
        return linkFirst;
    }

    public String getLinkLast() {
        return linkLast;
    }

    public String getImgStatusIcon() {
        return imgStatusIcon;
    }

    public String getUser() {
        return user;
    }

    public String getLinkNewPost() {
        return linkNewPost;
    }

    public void setLinkNewPost(String linkNewPost) {
        this.linkNewPost = linkNewPost.startsWith("http") ? linkNewPost: VozService.ENDPOINT + linkNewPost;
        this.link = this.linkNewPost;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = Integer.parseInt(totalPage);
    }

    public int getTotalPage() {
        return totalPage;
    }
    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = Integer.parseInt(currentPage);
    }

    public void next(){
        if(++currentPage > totalPage){
            currentPage = totalPage;
        }
        link = String.format("%sshowthread.php?t=%s&page=%d",VozService.ENDPOINT,id,currentPage);
    }

    public void last(){
        currentPage = totalPage;
        link = String.format("%sshowthread.php?t=%s&page=%d",VozService.ENDPOINT,id,currentPage);

    }
    public void previous(){
        if(--currentPage < 1){
            currentPage = 1;
        }
        link = String.format("%sshowthread.php?t=%s&page=%d",VozService.ENDPOINT,id,currentPage);

    }
    public void first(){
        currentPage = 1;
        link = String.format("%sshowthread.php?t=%s&page=%d",VozService.ENDPOINT,id,currentPage);

    }


    public void setTitle(String title) {
        this.title = title;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setLinkFirst(String linkFirst) {

        this.linkFirst = linkFirst.startsWith("http") ? linkFirst: VozService.ENDPOINT + linkFirst;
        this.link = this.linkFirst;

    }

    public void setLinkLast(String linkLast) {
        this.linkLast = linkLast.startsWith("http") ? linkLast: VozService.ENDPOINT + linkLast;
    }

    public void setImgStatusIcon(String imgStatusIcon) {
        this.imgStatusIcon = imgStatusIcon;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setReplies(String replies) {
        this.replies = replies;
    }

    public void setSticky(boolean sticky) {
        isSticky = sticky;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }
}
