package nna.vozforums.supervisor.data.local;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

import nna.vozforums.supervisor.injection.ApplicationContext;


public class PreferencesHelper {

    public static final String PREF_FILE_NAME = "esmilehotel_pref";
    public static final String PREF_DEVICE_IKY = "PREF_DEVICE_IKY";
    public static final String PREF_USER_COOKIE = "PREF_USER_COOKIE";
    public static final String PREF_USER_AVATAR = "PREF_USER_AVATAR";
    public static final String PREF_USER_NAME = "PREF_USER_NAME";
    public static final String PREF_USER_USERID = "PREF_USER_USERID" ;

    private final SharedPreferences mPref;

    @Inject
    public PreferencesHelper(@ApplicationContext Context context) {
        mPref = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
    }

    public void clear() {
        mPref.edit().clear().apply();
    }
    public void setValue(String key, String value){
        SharedPreferences.Editor editor = mPref.edit();
        editor.putString(key,value);
        editor.commit();
    }

    public void clearValue(String key){
        SharedPreferences.Editor editor = mPref.edit();
        editor.putString(key,null);
        editor.commit();

    }
    public String getValue(String key, String stringdefault){
        return mPref.getString(key,stringdefault);
    }

}
