package nna.vozforums.supervisor.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.jsoup.nodes.Element;

import nna.vozforums.supervisor.data.remote.VozService;

/**
 * Created by Ann on 3/2/16.
 */
public class Box implements Parcelable {

    public static final int NORMAL = 1;
    public static final int SPECIAL = 2;

    String name;
    String link;
    String views;
    int type;

    public Box(){

    }
    public Box(Element element) {

        if(element.select("td[class=tcat]").first() != null){
            type = SPECIAL;
            name = element.text();
            link = element.select("a").attr("href");
        }else{
            type = NORMAL;
            Element elealt1 = element.select("td[class^=alt1Active]").first();
            name = elealt1.select("a").text();
            link = elealt1.select("a").attr("href");
            link = link.startsWith("http") ? link: VozService.ENDPOINT + link;
            views = elealt1.select("span").text();
        }

    }

    protected Box(Parcel in) {
        name = in.readString();
        link = in.readString();
        views = in.readString();
        type = in.readInt();
    }

    public static final Creator<Box> CREATOR = new Creator<Box>() {
        @Override
        public Box createFromParcel(Parcel in) {
            return new Box(in);
        }

        @Override
        public Box[] newArray(int size) {
            return new Box[size];
        }
    };

    public void setLinkFromF(String f) {
        this.link = VozService.ENDPOINT + "forumdisplay.php?f=" + f;
    }


    public String getName() {
        return name;
    }

    public String getLink() {

        return link;
    }

    public String getViews() {
        return views;
    }

    public int getType() {
        return type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s",name,link,views);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(link);
        dest.writeString(views);
        dest.writeInt(type);
    }
}
