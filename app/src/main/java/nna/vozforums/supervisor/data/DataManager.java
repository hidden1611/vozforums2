package nna.vozforums.supervisor.data;

import org.jsoup.nodes.Document;

import java.util.List;

import javax.inject.Inject;

import nna.vozforums.supervisor.data.model.Box;
import nna.vozforums.supervisor.data.model.Topic;
import nna.vozforums.supervisor.data.remote.VozService;
import rx.Observable;

/**
 * Created by Ann on 3/2/16.
 */
public class DataManager {

    private final VozService vozService;

    @Inject
    public DataManager(VozService vozService){
        this.vozService = vozService;

    }

    public Observable<List<Box>> getHome(){
        return vozService.getHome();
    }

    public Observable<List<Topic>> listTopic(Box box){
        return vozService.listTopics(box);
    }
    public Observable<String> listPost(Topic topic){
        return vozService.listPosts(topic);
    }

    public Observable<String> login(){
        return vozService.login();
    }

    public Observable<String> avatar(){
        return vozService.getAvatar();
    }

    public Observable<Document> fetchData(String url){
        return vozService.fetchData(url);
    }


    public Observable<Document> postReply( String url,  String title,  String message,
                                           String token,  String idThread,  String p,  String specifiedPost,
                                           String postHash,  String postStartTime){

        return vozService.postReply(url,  title,  message, token,  idThread,  p,  specifiedPost, postHash,  postStartTime);
    }



}
