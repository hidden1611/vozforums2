package nna.vozforums.supervisor.data.model;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import nna.vozforums.supervisor.data.local.PreferencesHelper;

/**
 * Created by Ann on 3/16/16.
 */
@Singleton
public class User {

    PreferencesHelper preferencesHelper;

    String username;
    String password;

    String vfsessionhash;
    String vflastvisit;
    String vflastactivity;
    String vfuserid;
    String vfpassword;
    String vfimloggedin;

    String cookie;


    @Inject
    public User(PreferencesHelper preferencesHelper){

//        username = "hidden1611";
//        vfsessionhash = "120224d49f2174a23257a4388c896490";
//        vflastvisit = "1458117228";
//        vflastactivity = "0";
//        vfuserid = "953521";
//        vfpassword = "5392c2d1e47c6a2e7520307ee03a29e2";
//        vfimloggedin = "yes";
        this.preferencesHelper = preferencesHelper;
        vfuserid = preferencesHelper.getValue(PreferencesHelper.PREF_USER_USERID,"");

    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String generateMd5(){
        return "2cb73e096e8fd2cf0cb35bebc78fb819";
    }
    public String getJsonDataLogin(){
        return "{'vb_login_username':'" + username + "',"
                + "'cookieuser':'1',"
                + "'securitytoken':'guest',"
                + "'vb_login_md5password':'" + generateMd5() + "',"
                + "'vb_login_md5password_utf':'" + generateMd5() + "',"
                + "'do':'login'"
                + "}";
    }

    public String getCookies(){
        return preferencesHelper.getValue(PreferencesHelper.PREF_USER_COOKIE, null);
    }

    public String getUsername() {
        return username;
    }

    public void setCookies(List<String> headers){
        cookie = "";

        for (String header : headers) {

            if(header.split(";")[0].contains("vfuserid")){
                vfuserid = header.split(";")[0].split("=")[1];
            }

            cookie += header.split(";")[0] +"; ";

        }

        preferencesHelper.setValue(PreferencesHelper.PREF_USER_USERID, vfuserid);
        preferencesHelper.setValue(PreferencesHelper.PREF_USER_COOKIE, cookie);

    }

    public String getVfuserid() {
        return vfuserid;
    }


    /*

Set-Cookie: vfsessionhash=120224d49f2174a23257a4388c896490; path=/; domain=vozforums.com; HttpOnly
Set-Cookie: vflastvisit=1458117228; expires=Thu, 16-Mar-2017 08:33:48 GMT; path=/; domain=vozforums.com; secure
Set-Cookie: vflastactivity=0; expires=Thu, 16-Mar-2017 08:33:48 GMT; path=/; domain=vozforums.com; secure
Set-Cookie: vfuserid=953521; expires=Thu, 16-Mar-2017 08:33:48 GMT; path=/; domain=vozforums.com; secure; HttpOnly
Set-Cookie: vfpassword=5392c2d1e47c6a2e7520307ee03a29e2; expires=Thu, 16-Mar-2017 08:33:48 GMT; path=/; domain=vozforums.com; secure; HttpOnly
Set-Cookie: vfimloggedin=yes; expires=Thu, 16-Mar-2017 08:33:48 GMT; path=/; domain=vozforums.com; secure; HttpOnly
     */

}
