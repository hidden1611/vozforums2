package nna.vozforums.supervisor.data.remote;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import nna.vozforums.supervisor.data.model.Box;
import nna.vozforums.supervisor.data.model.Topic;
import nna.vozforums.supervisor.data.model.User;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by Ann on 3/2/16.
 */
@Singleton
public class VozService {

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    public static final String ENDPOINT = "https://vozforums.com/";

    private final OkHttpClient client;


    @Inject User user;

    @Inject
    public VozService(){
        client = new OkHttpClient().newBuilder()
                .followSslRedirects(true)
                .followRedirects(true).build();


    }

    String get(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Cookie", user.getCookies())
                .build();
        Response response = client.newCall(request).execute();
        String stringResponse = response.body().string();
        response.body().close();
        return stringResponse;
    }

    String get(String url,String cookie) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Cookie", cookie)
                .build();
        Response response = client.newCall(request).execute();
        String stringResponse = response.body().string();
        response.body().close();
        return stringResponse;
    }

    public Observable<List<Box>> getHome(){
        return Observable.create(new Observable.OnSubscribe<List<Box>>() {
            @Override
            public void call(Subscriber<? super List<Box>> subscriber) {
                try {
                    String response = get(ENDPOINT);
                    Document doc = Jsoup.parse(response);
                    Elements elements = doc.select("tr:has(td[class]:has(a[href^=forumdisplay]))");
                    elements.remove(elements.size()-1);
                    elements.remove(0);
                    elements.remove(0);
                    List<Box> boxes = new ArrayList<Box>();
                    for (Element element : elements) {
                        boxes.add(new Box(element));
                    }

                    subscriber.onNext(boxes);
                    subscriber.onCompleted();

                } catch (Exception e) {
                    e.printStackTrace();
                    subscriber.onError(e);
                }

            }
        });
    }

    public Observable<List<Topic>> listTopics(final Box box){

        return Observable.create(new Observable.OnSubscribe<List<Topic>>() {
            @Override
            public void call(Subscriber<? super List<Topic>> subscriber) {
                try {

                    String respone = get(box.getLink());
                    Document doc = Jsoup.parse(respone);
                    Elements elements = doc.select("tr:has(td[id*=td_threads])");
                    elements.remove(0);

                    List<Topic> topics = new ArrayList<Topic>();
                    for(Element element : elements){
                        topics.add(new Topic(element));
                    }
                    subscriber.onNext(topics);
                    subscriber.onCompleted();

                }catch (Exception e){
                    e.printStackTrace();
                    subscriber.onError(e);
                }

            }
        });
    }

    public Observable<String> listPosts(final Topic topic){
        return Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                try {
                    String response = get(topic.getLink());
                    subscriber.onNext(response);
                } catch (Exception e) {
                    e.printStackTrace();
                    subscriber.onError(e);
                }

            }
        });
    }

    public Observable<String> getAvatar() {
        return Observable.create(new Observable.OnSubscribe<String>() {

            @Override
            public void call(Subscriber<? super String> subscriber) {

                try {
                    String response = get("https://vozforums.com/profile.php?do=editavatar");
                    Document doc = Jsoup.parse(response);
                    Element ele = doc.select("img[alt=Custom Avatar]").first();
                    String avatar = ele.attr("src");
                    subscriber.onNext(avatar.startsWith("http")?avatar:ENDPOINT + avatar);
                    subscriber.onCompleted();

                } catch (Exception e) {
                    e.printStackTrace();
                    subscriber.onError(e);
                }

            }
        });
    }




    public Observable<String> login(){
        return Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {

                try {
                    RequestBody body = new FormBody.Builder()
                            .add("vb_login_username", user.getUsername())
                            .add("cookieuser", "1")
                            .add("securitytoken", "guest")
                            .add("vb_login_md5password", user.generateMd5())
                            .add("vb_login_md5password_utf", user.generateMd5())
                            .add("do","login")
                            .build();
                    Request request = new Request.Builder()
                            .url("https://vozforums.com/login.php?do=login")
                            .post(body)
                            .build();
                    Response response = client.newCall(request).execute();
                    user.setCookies(response.headers("Set-Cookie"));
                    response.body().close();
                    subscriber.onNext("OK");
                    subscriber.onCompleted();
                }catch (Exception e){
                    subscriber.onError(e);

                }

            }
        });
    }

    public Observable<Document> fetchData(final String url) {
        return Observable.create(new Observable.OnSubscribe<Document>() {

            @Override
            public void call(Subscriber<? super Document> subscriber) {

                try {
                    String response = get(url);
                    Document doc = Jsoup.parse(response);
                    subscriber.onNext(doc);
                    subscriber.onCompleted();

                } catch (Exception e) {
                    e.printStackTrace();
                    subscriber.onError(e);
                }

            }
        });
    }

    public Observable<Document> postReply(final String url, final String title, final String message,
                                          final String token, final String idThread, final String p, final String specifiedPost,
                                          final String postHash, final String postStartTime) {
        return Observable.create(new Observable.OnSubscribe<Document>() {

            @Override
            public void call(Subscriber<? super Document> subscriber) {

                try {

                    /*
                     Connection.Response res = Jsoup.connect(mUrl)
                            .timeout(TIMEOUT)
                            .cookies(mUser.cookies())
                            .data("title:Re: ",title)
                            .data("message",message)
                            .data("wysiwyg","0")
                            .data("s", " ")
                            .data("securitytoken",mUser.Token())
                            .data("do", "postreply")
                            .data("t", thread)
                            .data("p", p)
                            .data("specifiedpost", specifiedpost)
                            .data("posthash", postHash)
                            .data("poststarttime", postStartTime)
                            .data("loggedinuser", mUser.UserId())
                            .data("multiquoteempty:", " ")
                            .data("sbutton", "Submit GetReply")
                            .data("signature","1")
                            .data("parseurl", "1")
                            .method(Connection.Method.POST)
                            .execute();
                     */

                    RequestBody fromBody = new FormBody.Builder()
                            .add("title:Re: ",title)
                            .add("message",message)
                            .add("wysiwyg","0")
                            .add("s", " ")
                            .add("securitytoken", token)
                            .add("do", "postreply")
                            .add("t", idThread)
                            .add("p", p)
                            .add("specifiedpost", specifiedPost)
                            .add("posthash", postHash)
                            .add("poststarttime", postStartTime)
                            .add("loggedinuser", user.getVfuserid())
                            .add("multiquoteempty:", " ")
                            .add("sbutton", "Submit Reply")
                            .add("signature","1")
                            .add("parseurl", "1")
                            .build();

                    Request request = new Request.Builder()
                            .url(url)
                            .addHeader("Cookie", user.getCookies())
                            .post(fromBody)
                            .build();

                    Response response = client.newCall(request).execute();
                    String stringResponse = response.body().string();
                    response.body().close();

                    subscriber.onNext(new Document(stringResponse));
                    subscriber.onCompleted();


                } catch (Exception e) {
                    e.printStackTrace();
                    subscriber.onError(e);
                }

            }
        });
    }

    private static final String TAG = "VozService";




}
