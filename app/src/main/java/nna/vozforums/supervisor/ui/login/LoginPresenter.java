package nna.vozforums.supervisor.ui.login;

import javax.inject.Inject;

import nna.vozforums.supervisor.data.BusEvent;
import nna.vozforums.supervisor.data.DataManager;
import nna.vozforums.supervisor.data.local.PreferencesHelper;
import nna.vozforums.supervisor.data.model.User;
import nna.vozforums.supervisor.ui.base.BasePresenter;
import nna.vozforums.supervisor.util.EventPosterHelper;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LoginPresenter extends BasePresenter<LoginMvpView> {

    DataManager dataManager;
    @Inject
    User user;

    @Inject
    EventPosterHelper eventPosterHelper;
    @Inject
    PreferencesHelper preferencesHelper;

    @Inject
    public LoginPresenter(DataManager dataManager) {

        this.dataManager = dataManager;

    }

    @Override
    public void attachView(LoginMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public void login(final String username, String pass){

        if (isViewAttached()) {
            getMvpView().showProcessbar(true);
        }
        user.setUsername(username);
        user.setPassword(pass);
        dataManager.login()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {
                        if (isViewAttached()) {
                            getMvpView().showProcessbar(false);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                        e.printStackTrace();
                        if (isViewAttached()) {
                            getMvpView().showProcessbar(false);
                        }

                    }

                    @Override
                    public void onNext(String data) {

                        preferencesHelper.setValue(PreferencesHelper.PREF_USER_NAME, username);
                        getAvatar();
                    }
                });
    }


    public void getAvatar(){


        dataManager.avatar()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        e.printStackTrace();


                    }

                    @Override
                    public void onNext(String data) {

                        if(isViewAttached()){

                            preferencesHelper.setValue(PreferencesHelper.PREF_USER_AVATAR, data);
                            eventPosterHelper.postEventSafely(new BusEvent.UpdateAvatar(data));

                        }

                    }
                });
    }

    private static final String TAG = "LoginPresenter";



}

