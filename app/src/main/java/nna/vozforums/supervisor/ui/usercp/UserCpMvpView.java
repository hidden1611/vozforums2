package nna.vozforums.supervisor.ui.usercp;

import java.util.List;

import nna.vozforums.supervisor.data.model.Topic;
import nna.vozforums.supervisor.ui.base.MvpView;

public interface UserCpMvpView extends MvpView {

    void showProcessbar(boolean show);

    void updateBoxes(List<Topic> topics);

    void showError(String message);
}
