package nna.vozforums.supervisor.ui.usercp;

import javax.inject.Inject;

import nna.vozforums.supervisor.ui.base.BaseActivity;
import nna.vozforums.supervisor.ui.base.BaseTopicFragment;

public class UserCpFragment extends BaseTopicFragment implements UserCpMvpView{

    @Inject
    UserCpPresenter mUserCpPresenter;

    @Override
    public void inject() {
        ((BaseActivity) getActivity()).activityComponent().inject(this);
    }


    @Override
    public void attachPresenter() {

        mUserCpPresenter.attachView(this);
        setTitle("User CP");
    }

    @Override
    public void detachPresenter() {

        mUserCpPresenter.detachView();

    }

    @Override
    public void getData() {

        mUserCpPresenter.getData();

    }


}
