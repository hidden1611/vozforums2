package nna.vozforums.supervisor.ui.box;

import android.content.Context;

import java.util.List;

import javax.inject.Inject;

import nna.vozforums.supervisor.VozApplication;
import nna.vozforums.supervisor.data.DataManager;
import nna.vozforums.supervisor.data.model.Box;
import nna.vozforums.supervisor.injection.ApplicationContext;
import nna.vozforums.supervisor.ui.base.BasePresenter;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Ann on 3/12/16.
 */
public class BoxPresenter extends BasePresenter<BoxMvpView> {

    @Inject
    DataManager dataManager;

    List<Box> boxes;
    @Inject
    public BoxPresenter(@ApplicationContext Context context) {
        VozApplication.get(context).getComponent().inject(this);
    }

    @Override
    public void attachView(BoxMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public void getData(){
        if(isViewAttached()){
            getMvpView().showLoading();
        }
        dataManager.getHome()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<Box>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(isViewAttached()){
                            getMvpView().hideLoading();
                        }

                    }

                    @Override
                    public void onNext(List<Box> box) {
                        if(isViewAttached()){

                            boxes = box;
                            getMvpView().setBoxes(box);
                            getMvpView().hideLoading();
                        }

                    }
                });
    }
}
