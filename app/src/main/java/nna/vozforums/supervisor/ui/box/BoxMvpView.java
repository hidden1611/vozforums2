package nna.vozforums.supervisor.ui.box;

import java.util.List;

import nna.vozforums.supervisor.data.model.Box;
import nna.vozforums.supervisor.ui.base.MvpView;

/**
 * Created by Ann on 3/12/16.
 */
public interface BoxMvpView extends MvpView{

    void addBox(Box box);

    void setBoxes(List<Box> boxes);

    void showLoading();

    void hideLoading();
}
