package nna.vozforums.supervisor.ui.usercp;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import nna.vozforums.supervisor.data.DataManager;
import nna.vozforums.supervisor.data.model.Topic;
import nna.vozforums.supervisor.data.remote.VozApi;
import nna.vozforums.supervisor.ui.base.BasePresenter;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class UserCpPresenter extends BasePresenter<UserCpMvpView> {

    DataManager dataManager;

    @Inject
    public UserCpPresenter(DataManager dataManager) {

        this.dataManager = dataManager;

    }

    @Override
    public void attachView(UserCpMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }


    public void getData(){

        if (isViewAttached()) {
            getMvpView().showProcessbar(true);
        }

        dataManager.fetchData(VozApi.userCp())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Document>() {
                    @Override
                    public void onCompleted() {
                        if (isViewAttached()) {
                            getMvpView().showProcessbar(false);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                        e.printStackTrace();
                        if (isViewAttached()) {
                            getMvpView().showProcessbar(false);
                        }

                    }

                    @Override
                    public void onNext(Document data) {


                        if(isViewAttached()){
                            getMvpView().updateBoxes(parserData(data));
                        }

                    }
                });
    }


    private List<Topic> parserData(Document document){


        List<Topic> listBox = new ArrayList<>();


        Elements eles = document.select("tr:has(td[id*=td_threads])");
        eles.remove(0);
        for(Element eleTag1:eles){
            try {
                Topic topic = new Topic();
                if (eleTag1.select("a[id*=thread_title").first() != null) {
                    topic.setName(eleTag1.select("a[id*=thread_title").first().text());
                    topic.setLinkFirst(eleTag1.select("a[id*=thread_title").first().attr("href"));
                }

                Element eleLastPost = eleTag1.select("td[title*=Replies]").first();
                if (eleLastPost != null) {
                    String text2 = null;
                    Element elettemp = eleLastPost.select("div").first();
                    if (elettemp != null)
                        text2 = elettemp.ownText() + " " + elettemp.select("a[href*=member.php]").text();
                    topic.setUser(eleTag1.select("div:has(span[onclick*=member.php])").first().text());
                    topic.setTime(text2);
                    topic.setReplies(eleLastPost.attr("title").replace(", ","\n"));

                    if (eleLastPost.select("a[href*=showthread]").first() != null) {
                        topic.setLinkLast(eleLastPost.select("a[href*=showthread]").attr("href"));
                    }
                }
                try {
                    topic.setTitle(eleTag1.select("td[title]").get(0).attr("title"));
                }catch (Exception e){
                    e.printStackTrace();
                }

                if (eleTag1.select("a[id*=thread_gotonew]").first() != null) {
                    topic.setLinkNewPost(eleTag1.select("a[id*=thread_gotonew]").first().attr("href"));
                }
                topic.setId(topic.getLinkFirst().split("t=")[1]);
//            if (checkBookmark(topic.getmIdThread())) {
//                topic.setIsBookmark(true);
//                mDataBookmark.updateBookmark(topic);
//            }
                listBox.add(topic);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        return listBox;

    }


}

