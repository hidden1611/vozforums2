package nna.vozforums.supervisor.ui.box;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import nna.vozforums.supervisor.R;
import nna.vozforums.supervisor.data.model.Box;
import nna.vozforums.supervisor.ui.base.BaseActivity;
import nna.vozforums.supervisor.ui.base.BaseFragment;
import nna.vozforums.supervisor.ui.main.MainActivity;
import nna.vozforums.supervisor.ui.topic.TopicFragment;

/**
 * Created by Ann on 3/12/16.
 */
public class BoxFragment extends BaseFragment implements BoxMvpView, BoxAdapter.OnItemClickListener, SwipeRefreshLayout.OnRefreshListener {


    @Inject BoxPresenter boxPresenter;

    @BindView(R.id.rv)
    RecyclerView rv;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    BoxAdapter boxAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((BaseActivity)getActivity()).activityComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_box, container, false);
        ButterKnife.bind(this, fragmentView);

        getActivity().setTitle("vozForums");
        swipeRefreshLayout.setOnRefreshListener(this);
        boxPresenter.attachView(this);
        initRecyclerview();
        if (!isCallback) {
            boxPresenter.getData();
        }else {
            setBoxes(boxPresenter.boxes);

        }

        isCallback = true;
        return fragmentView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        boxPresenter.detachView();
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if(menuVisible){
            getActivity().setTitle("aaa");
        }
    }

    private void initRecyclerview() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv.setLayoutManager(linearLayoutManager);
        boxAdapter = new BoxAdapter();
        boxAdapter.setOnItemClickListener(this);
        rv.setAdapter(boxAdapter);
    }

    @Override
    public void addBox(final Box box) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                boxAdapter.addItem(box);
            }
        });

    }

    @Override
    public void setBoxes(final List<Box> boxes) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                boxAdapter.setItems(boxes);

            }
        });
    }

    @Override
    public void showLoading() {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {

                swipeRefreshLayout.setRefreshing(true);
            }
        });
    }

    @Override
    public void hideLoading() {

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);

            }
        });

    }

    @Override
    public void OnClick(View view, int position) {

        TopicFragment topicFragment = new TopicFragment();
        topicFragment.setBox(boxAdapter.getItem(position));
        ((MainActivity)getActivity()).addFragment(topicFragment);


    }

    @Override
    public void onRefresh() {
        boxAdapter.clear();
        boxPresenter.getData();

    }


}
