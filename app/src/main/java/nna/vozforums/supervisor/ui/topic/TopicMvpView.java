package nna.vozforums.supervisor.ui.topic;

import java.util.List;

import nna.vozforums.supervisor.data.model.Topic;
import nna.vozforums.supervisor.ui.base.MvpView;

/**
 * Created by Ann on 3/13/16.
 */
public interface TopicMvpView extends MvpView{

    void showProcessbar(boolean show);

    void updateBoxes(List<Topic> topics);

    void showError(String message);
}
