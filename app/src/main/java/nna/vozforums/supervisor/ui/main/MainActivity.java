package nna.vozforums.supervisor.ui.main;

import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nna.vozforums.supervisor.R;
import nna.vozforums.supervisor.data.BusEvent;
import nna.vozforums.supervisor.data.local.PreferencesHelper;
import nna.vozforums.supervisor.data.model.Box;
import nna.vozforums.supervisor.ui.base.BaseActivity;
import nna.vozforums.supervisor.ui.box.BoxFragment;
import nna.vozforums.supervisor.ui.login.LoginFragment;
import nna.vozforums.supervisor.ui.topic.TopicFragment;
import nna.vozforums.supervisor.ui.usercp.UserCpFragment;
import nna.vozforums.supervisor.util.ViewUtil;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, MainMvpView {

    private static final String TAG = "MainActivity";

    @Inject MainPresenter mainPresenter;
    @Inject
    PreferencesHelper preferencesHelper;


    @Inject
    Bus bus;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    public static int WIDTH;
    private TextView tvLogin;
    private ImageView ivAvatar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        mainPresenter.attachView(this);

        setContentView(R.layout.activity_main);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment, new BoxFragment() )
                .commit();

        Point size = new Point();
        WindowManager w = getWindowManager();
        w.getDefaultDisplay().getSize(size);
        WIDTH = size.x - ViewUtil.dpToPx(32);

        View viewHeaderNav = navigationView.getHeaderView(0);
        tvLogin = (TextView)viewHeaderNav.findViewById(R.id.tvLogin);
        ivAvatar = (ImageView)viewHeaderNav.findViewById(R.id.ivAvatar);
        viewHeaderNav.findViewById(R.id.ivSetting).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                replaceFragment(new LoginFragment());
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });

        Glide.with(this)
                .load(preferencesHelper.getValue(PreferencesHelper.PREF_USER_AVATAR,""))
                .placeholder(R.drawable.ic_account_circle_white_24dp)
                .into(ivAvatar);
        tvLogin.setText(preferencesHelper.getValue(PreferencesHelper.PREF_USER_NAME,"Not Login"));
        bus.register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mainPresenter.detachView();
        bus.unregister(this);

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_forums) {
            showDialogForums();

        }else if(id == R.id.nav_cp){
            addFragment(new UserCpFragment());
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @OnClick ({R.id.tvQuickf1, R.id.tvQuickf2, R.id.tvQuickf3, R.id.tvQuickf4, R.id.tvQuickf5})
    public void onClickedQuick(View v){

        TopicFragment topicFragment = new TopicFragment();
        Box box = new Box();
        switch (v.getId()) {

            case R.id.tvQuickf1:

                box.setLinkFromF("0");
                topicFragment.setBox(box);
                addFragment(topicFragment);
                break;


            case R.id.tvQuickf2:
                box.setLinkFromF("17");
                topicFragment.setBox(box);
                addFragment(topicFragment);
                break;

            case R.id.tvQuickf3:
                box.setLinkFromF("26");
                topicFragment.setBox(box);
                addFragment(topicFragment);
                break;


            case R.id.tvQuickf4:

                box.setLinkFromF("27");
                topicFragment.setBox(box);
                addFragment(topicFragment);
                break;


            case R.id.tvQuickf5:

                box.setLinkFromF("34");
                topicFragment.setBox(box);
                addFragment(topicFragment);
                break;
        }

    }


    public void addFragment(Fragment fragment){
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment, fragment )
                .addToBackStack(null)
                .commit();
    }

    public void replaceFragment(Fragment fragment){

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment, fragment )
                .addToBackStack(null)
                .commit();

    }


    @Override
    public void showToast(String toast) {

        Toast.makeText(MainActivity.this, toast, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void showProcessbar(boolean show) {

    }

    private void showDialogForums(){
        new MaterialDialog.Builder(this)
                .title(R.string.dialog_forum_title)
                .inputType(InputType.TYPE_CLASS_NUMBER)
                .input("17", null, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {

                        if(TextUtils.isEmpty(input.toString())){
                            showToast("Id Box không tồn tại!");
                            return;

                        }
                        TopicFragment topicFragment = new TopicFragment();
                        Box box = new Box();
                        box.setLinkFromF(input.toString());
                        topicFragment.setBox(box);
                        addFragment(topicFragment);

                    }
                })
                .negativeText(android.R.string.cancel)
                .show();
    }

    @Subscribe
    public void eventBusAvatar(BusEvent.UpdateAvatar updateAvatar){

        Glide.with(this)
                .load(updateAvatar.avatart)
                .into(ivAvatar);

    }
}
