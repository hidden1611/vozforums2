package nna.vozforums.supervisor.ui.post;

import android.content.Context;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import nna.vozforums.supervisor.VozApplication;
import nna.vozforums.supervisor.data.DataManager;
import nna.vozforums.supervisor.data.model.Post;
import nna.vozforums.supervisor.data.model.Topic;
import nna.vozforums.supervisor.injection.ApplicationContext;
import nna.vozforums.supervisor.ui.base.BasePresenter;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Ann on 3/13/16.
 */
public class PostPresenter extends BasePresenter<PostMvpView> {



    @Inject
    DataManager dataManager;
    private Topic topic;

    @Inject
    public PostPresenter(@ApplicationContext Context context) {

        VozApplication.get(context).getComponent().inject(this);

    }


    public void getData(){
        if(isViewAttached()){
            getMvpView().showLoading();
        }

        dataManager.listPost(topic)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if(isViewAttached()){
                            e.printStackTrace();
                            getMvpView().hideLoading();
                            getMvpView().showError(e.getMessage());

                        }

                    }

                    @Override
                    public void onNext(String string) {
                        if(isViewAttached()){

                            parser(string);
                            getMvpView().hideLoading();
                        }


                    }
                });

    }


    private void parser(String data){
        Document doc = Jsoup.parse(data);
        List<Post> posts = new ArrayList<>();
        for (Element element : doc.select("table[id^=post]")) {

            try {
                posts.add(new Post(element));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //parser page
        Element elePage = doc.select("div[class=pagenav").first();
        if(elePage != null) {
            String page = elePage.select("td[class=vbmenu_control]").text();
            topic.setCurrentPage(page.split(" ")[1]);
            topic.setTotalPage(page.split(" ")[3]);
        }else {
            topic.setCurrentPage("1");
            topic.setTotalPage("1");

        }
        getMvpView().setPosts(posts);

    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }
}
