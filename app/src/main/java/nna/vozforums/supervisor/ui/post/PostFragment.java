package nna.vozforums.supervisor.ui.post;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import nna.vozforums.supervisor.R;
import nna.vozforums.supervisor.data.model.Post;
import nna.vozforums.supervisor.data.model.Topic;
import nna.vozforums.supervisor.ui.base.BaseActivity;
import nna.vozforums.supervisor.ui.base.BaseFragment;
import nna.vozforums.supervisor.ui.dialog.QuickMenuFragment;
import nna.vozforums.supervisor.ui.reply.ReplyFragment;
import nna.vozforums.supervisor.ui.reply.ReplyPresenter;


/**
 * Created by Ann on 3/13/16.
 */
public class PostFragment extends BaseFragment implements PostMvpView, SwipeRefreshLayout.OnRefreshListener {


    @Inject
    PostPresenter postPresenter;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.rv)
    RecyclerView rv;

    private PostAdapter postAdapter;
    private Topic topic;


    @Override

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((BaseActivity) getActivity()).activityComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_post, container, false);
        ButterKnife.bind(this, fragmentView);
        swipeRefreshLayout.setOnRefreshListener(this);
        initRecyclerview();
        postPresenter.attachView(this);
        postPresenter.setTopic(topic);
        postPresenter.getData();


        return fragmentView;
    }
    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if(menuVisible){

            getActivity().setTitle(topic.getName());

        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        postPresenter.detachView();
    }

    @Override
    public void setPosts(List<Post> posts) {
        postAdapter.setItems(posts);
    }

    @Override
    public void showLoading() {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {

                swipeRefreshLayout.setRefreshing(true);
            }
        });
    }

    @Override
    public void hideLoading() {

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);

            }
        });

    }

    @Override
    public void showError(String s) {
        Toast.makeText(getContext(), "" +  s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRefresh() {

        postAdapter.clear();
        postPresenter.getData();

    }



    private void initRecyclerview() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv.setLayoutManager(linearLayoutManager);
        postAdapter  = new PostAdapter(topic);
        rv.setAdapter(postAdapter);
        postAdapter.setOnItemClickListener(new PostAdapter.OnItemClickListener() {
            @Override
            public void OnClickItem(View view, int position) {

                QuickMenuFragment quickMenuFragment = new QuickMenuFragment();
                quickMenuFragment.show(getActivity().getSupportFragmentManager(), quickMenuFragment.getTag());

            }

            @Override
            public void OnClickPage(int type) {
                if(type == PostAdapter.PAGE_FAST_PREV){
                    topic.first();
                }else if(type == PostAdapter.PAGE_PREV){
                    topic.previous();
                }else if(type == PostAdapter.PAGE_NEXT){
                    topic.next();
                }else if(type == PostAdapter.PAGE_FAST_NEXT){
                    topic.last();
                }

                onRefresh();


            }

            @Override
            public void OnClickReply(View view, int position) {

                BottomSheetDialogFragment bottomSheetDialogFragment = ReplyFragment.newInstance(topic.getName(),
                                topic.getId(),
                                ReplyPresenter.TYPE_REPLY_QUOTE,
                                postAdapter.getItem(position - 1).getIdPost());

                bottomSheetDialogFragment.show(getActivity().getSupportFragmentManager(), bottomSheetDialogFragment.getTag());

            }

            @Override
            public void OnClickQuote(View view, int position) {

            }
        });
    }

    public void setTopic(Topic topic) {

        this.topic = topic;
    }

}
