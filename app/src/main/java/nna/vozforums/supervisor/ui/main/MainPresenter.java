package nna.vozforums.supervisor.ui.main;

import android.content.Context;

import javax.inject.Inject;

import nna.vozforums.supervisor.VozApplication;
import nna.vozforums.supervisor.data.DataManager;
import nna.vozforums.supervisor.injection.ApplicationContext;
import nna.vozforums.supervisor.ui.base.BasePresenter;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Ann on 3/12/16.
 */
public class MainPresenter extends BasePresenter<MainMvpView> {

    @Inject
    DataManager dataManager;

    @Inject
    public MainPresenter(@ApplicationContext Context context) {
        VozApplication.get(context).getComponent().inject(this);
    }

    @Override
    public void attachView(MainMvpView mvpView) {
        super.attachView(mvpView);
//        login();
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    private void login(){
        dataManager.login()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(String s) {

                    }
                });
    }

}
