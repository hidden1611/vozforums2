package nna.vozforums.supervisor.ui.topic;

import android.content.Context;

import java.util.List;

import javax.inject.Inject;

import nna.vozforums.supervisor.VozApplication;
import nna.vozforums.supervisor.data.DataManager;
import nna.vozforums.supervisor.data.model.Box;
import nna.vozforums.supervisor.data.model.Topic;
import nna.vozforums.supervisor.injection.ApplicationContext;
import nna.vozforums.supervisor.ui.base.BasePresenter;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Ann on 3/13/16.
 */
public class TopicPresenter extends BasePresenter<TopicMvpView>{

    @Inject
    DataManager dataManager;
    private Box box;

    @Inject
    public TopicPresenter(@ApplicationContext Context context) {
        VozApplication.get(context).getComponent().inject(this);
    }

    @Override
    public void attachView(TopicMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }
    public void setBox(Box box){
        this.box = box;
    }

    public void getData(){

        if(isViewAttached()){
            getMvpView().showProcessbar(true);
        }

        dataManager.listTopic(box)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<Topic>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if(isViewAttached()){
                            getMvpView().showProcessbar(false);
                            getMvpView().showError(e.getMessage());
                        }

                    }

                    @Override
                    public void onNext(List<Topic> topics) {
                        if(isViewAttached()){
                            getMvpView().updateBoxes(topics);
                            getMvpView().showProcessbar(false);
                        }

                    }
                });
    }


}
