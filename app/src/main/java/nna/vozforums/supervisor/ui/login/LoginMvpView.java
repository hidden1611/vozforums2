package nna.vozforums.supervisor.ui.login;

import nna.vozforums.supervisor.ui.base.MvpView;

public interface LoginMvpView extends MvpView {

    void showProcessbar(boolean show);

}
