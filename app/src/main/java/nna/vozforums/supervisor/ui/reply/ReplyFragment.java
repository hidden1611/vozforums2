package nna.vozforums.supervisor.ui.reply;

import android.app.Dialog;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.view.Display;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nna.vozforums.supervisor.R;
import nna.vozforums.supervisor.ui.base.BaseActivity;
import nna.vozforums.supervisor.ui.main.MainActivity;

public class ReplyFragment extends BottomSheetDialogFragment implements ReplyMvpView {

    private static final String BUNDLE_TITLE = "BUNDLE_TITLE";
    private static final String BUNDLE_QUOTE = "BUNDLE_QUOTE";
    private static final String BUNDLE_TYPE_REPLY = "BUNDLE_TYPE_REPLY";
    private static final String BUNDLE_THREADID = "BUNDLE_THREADID";

    @Inject ReplyPresenter replyPresenter;


    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.etReply)
    EditText etReply;

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {
 
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }

        }
 
        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };



    public static ReplyFragment newInstance(String threadId, String title) {

        Bundle args = new Bundle();
        args.putString(BUNDLE_TITLE, title);
        args.putString(BUNDLE_THREADID, threadId);

        ReplyFragment fragment = new ReplyFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static ReplyFragment newInstance ( String title, String threadId, int typeReply, String quote) {
        
        Bundle args = new Bundle();
        args.putString(BUNDLE_THREADID, threadId);
        args.putString(BUNDLE_TITLE, title);
        args.putString(BUNDLE_QUOTE, quote);
        args.putInt(BUNDLE_TYPE_REPLY, typeReply);

        ReplyFragment fragment = new ReplyFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((BaseActivity)getActivity()).activityComponent().inject(this);
        replyPresenter.attachView(this);
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.fragment_bottom_sheet, null);
        ButterKnife.bind(this, contentView);
        dialog.setContentView(contentView);

        title.setText(getArguments().getString(BUNDLE_TITLE));
        replyPresenter.setThreadId(getArguments().getString(BUNDLE_THREADID));

        if(getArguments().getString(BUNDLE_QUOTE) != null){
            replyPresenter.setIdQuote(getArguments().getString(BUNDLE_QUOTE));
            replyPresenter.setType(getArguments().getInt(BUNDLE_TYPE_REPLY));
        }else {
            replyPresenter.setType(ReplyPresenter.TYPE_REPLY_QUICK);
        }


        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        if( behavior != null && behavior instanceof BottomSheetBehavior ) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
            Display display = getActivity().getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            ((BottomSheetBehavior) behavior).setPeekHeight(size.y);
        }

        replyPresenter.getMessageReply();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        replyPresenter.detachView();

    }

    @OnClick({R.id.camera_button, R.id.link_button, R.id.butReplyEmo, R.id.btSummit, R.id.btCancel})
    public void OnClicked(View v){
        switch (v.getId()) {

            case R.id.camera_button:
                break;
            case R.id.link_button:
                break;
            case R.id.butReplyEmo:
                break;

            case R.id.btSummit:
                replyPresenter.postReply(etReply.getText().toString());
                break;
            case R.id.btCancel:
                dismiss();
                break;
        }
    }

    @Override
    public void updateQuote(String wholeText) {

        etReply.setText(wholeText + "\n");
        etReply.setSelection(etReply.getText().length());

    }

    @Override
    public void showProcessbar(boolean show) {

        ((MainActivity) getActivity()).showProcessbar(show);

    }

    @Override
    public void showToast(String s) {
        Toast.makeText(getActivity(), s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void updateReplySuccess() {

        getActivity().onBackPressed();

    }
}