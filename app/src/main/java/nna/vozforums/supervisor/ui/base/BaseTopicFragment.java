package nna.vozforums.supervisor.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import nna.vozforums.supervisor.R;
import nna.vozforums.supervisor.data.model.Topic;
import nna.vozforums.supervisor.ui.main.MainActivity;
import nna.vozforums.supervisor.ui.post.PostFragment;
import nna.vozforums.supervisor.ui.topic.TopicAdapter;

/**
 * Created by Ann on 1/11/17.
 */

public abstract class BaseTopicFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.rv)
    RecyclerView rv;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    private TopicAdapter topicAdapter;
    List<Topic> topics;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inject();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.fragment_user_cp, container, false);
        ButterKnife.bind(this, view);
        setTitle("vozForums");
        swipeRefreshLayout.setOnRefreshListener(this);
        attachPresenter();
        initRecyclerView();

        if (!isCallback) {
            getData();
        }else {
            updateBoxes(topics);
        }
        isCallback = true;

        return view;
    }

    abstract public void inject();
    abstract public void attachPresenter();
    abstract public void detachPresenter();
    abstract public void getData();


    private void initRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv.setLayoutManager(linearLayoutManager);
        topicAdapter = new TopicAdapter();
        topicAdapter.setOnItemClickListener(new TopicAdapter.OnItemClickListener() {
            @Override
            public void OnClick(View view, int position) {

                Topic topic = topicAdapter.getItem(position);
                PostFragment postFragment = new PostFragment();
                postFragment.setTopic(topic);
                ((MainActivity)getActivity()).addFragment(postFragment);
            }
        });
        rv.setAdapter(topicAdapter);

    }


    @Override
    public void onDestroyView() {

        super.onDestroyView();
        detachPresenter();

    }

    public void showProcessbar(final boolean show) {

        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {

                swipeRefreshLayout.setRefreshing(show);
            }
        });

    }

    public void updateBoxes(List<Topic> boxes) {

        topics = boxes;
        topicAdapter.setItems(boxes);

    }



    public void showError(String message){

        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();

    }

    @Override
    public void onRefresh() {

        topicAdapter.clear();
        getData();

    }


}
