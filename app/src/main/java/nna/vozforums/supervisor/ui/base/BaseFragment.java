package nna.vozforums.supervisor.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import nna.vozforums.supervisor.ui.main.MainActivity;

/**
 * Created by Ann on 1/6/17.
 */

public class BaseFragment extends Fragment {

    public boolean isCallback = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isCallback = false;
    }

    public void setTitle(String title){
        getActivity().setTitle(title);
    }

    public void showProcessbar(boolean show){

        ((MainActivity) getActivity()).showProcessbar(show);
    }



}
