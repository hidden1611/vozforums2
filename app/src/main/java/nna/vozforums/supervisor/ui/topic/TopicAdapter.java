package nna.vozforums.supervisor.ui.topic;

import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import nna.vozforums.supervisor.R;
import nna.vozforums.supervisor.data.model.Topic;

/**
 * Created by Ann on 3/13/16.
 */
public class TopicAdapter extends RecyclerView.Adapter<TopicAdapter.TopicViewHolder> {

    private List<Topic> items;

    public TopicAdapter() {
        this.items = new ArrayList<>();
    }

    public void addItem(Topic item) {
        items.add(item);
        notifyDataSetChanged();
    }

    public void setItems(List<Topic> topics){
        items.clear();
        items = topics;
        notifyDataSetChanged();
    }

    public Topic getItem(int index) {
        return items.get(index);
    }

    public List<Topic> getItems() {
        return items;
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }


    @Override
    public TopicViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_topic, parent, false);

        return new TopicViewHolder(v);
    }

    @Override
    public void onBindViewHolder(TopicViewHolder holder, int position) {

        holder.setValue(items.get(position));

    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public static class TopicViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvTime)
        TextView tvTime;

        public TopicViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.OnClick(v, getLayoutPosition());
                    }
                }
            });
        }

        public void setValue(Topic item) {
            tvName.setText(item.getName());
            tvTitle.setText(item.getTitle());
            tvTime.setText(item.getTime());

            if(item.getLinkNewPost() != null){
                tvName.setTypeface(Typeface.DEFAULT_BOLD);
            }else {
                tvName.setTypeface(Typeface.DEFAULT);
            }

            tvName.setTextColor(item.isSticky()? Color.RED : Color.BLACK);

        }
    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public static OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void OnClick(View view, int position);
    }
}
