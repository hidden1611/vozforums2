package nna.vozforums.supervisor.ui.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nna.vozforums.supervisor.R;
import nna.vozforums.supervisor.ui.base.BaseActivity;
import nna.vozforums.supervisor.ui.main.MainActivity;

public class LoginFragment extends Fragment implements LoginMvpView {

    @Inject
    LoginPresenter mLoginPresenter;

    @BindView(R.id.etUser)
    EditText etUser;

    @BindView(R.id.etPass)
    EditText etPass;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
        ((BaseActivity) getActivity()).activityComponent().inject(this);
        getActivity().setTitle(R.string.login);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);

        mLoginPresenter.attachView(this);
        return view;
    }


    @Override
    public void onDestroyView() {

        super.onDestroyView();
        mLoginPresenter.detachView();

    }

    @Override
    public void showProcessbar(boolean show) {

        ((MainActivity) getActivity()).showProcessbar(show);

    }

    @OnClick({R.id.login_button, R.id.cancel_action})
    public void OnClicked(View v){
        switch (v.getId()) {
            
            case R.id.cancel_action:
                getActivity().onBackPressed();
                break;

            case R.id.login_button:
                mLoginPresenter.login(etUser.getText().toString(), etPass.getText().toString());
                break;
        }
    }
}
