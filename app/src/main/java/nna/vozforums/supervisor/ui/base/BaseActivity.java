package nna.vozforums.supervisor.ui.base;

import android.annotation.TargetApi;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import nna.vozforums.supervisor.VozApplication;
import nna.vozforums.supervisor.injection.component.ActivityComponent;
import nna.vozforums.supervisor.injection.component.DaggerActivityComponent;
import nna.vozforums.supervisor.injection.module.ActivityModule;


public class BaseActivity extends AppCompatActivity {

    private ActivityComponent mActivityComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public ActivityComponent activityComponent() {
        if (mActivityComponent == null) {
            mActivityComponent = DaggerActivityComponent.builder()
                    .activityModule(new ActivityModule(this))
                    .applicationComponent(VozApplication.get(this).getComponent())
                    .build();
        }
        return mActivityComponent;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermissionsSafely(String[] permissions, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermission(String permission) {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }


    public void addFragment(int layout, Fragment fragment){
        getSupportFragmentManager().beginTransaction()
                .add(layout, fragment)
                .commit();
    }

    public void replaceFragment(int layout, Fragment fragment){
        getSupportFragmentManager().beginTransaction()
                .replace(layout, fragment)
                .commit();
    }

    public void replaceFragmentWithBack(int layout, Fragment fragment){
        getSupportFragmentManager().beginTransaction()
                .replace(layout, fragment)
                .addToBackStack(null)
                .commit();
    }


}
