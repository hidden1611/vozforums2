package nna.vozforums.supervisor.ui.post;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import nna.vozforums.supervisor.R;
import nna.vozforums.supervisor.data.model.Post;
import nna.vozforums.supervisor.data.model.Topic;
import nna.vozforums.supervisor.util.HtmlTagHandler;
import nna.vozforums.supervisor.util.PicassoImageGetter;

/**
 * Created by Ann on 3/13/16.
 */
public class PostAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;

    public static final int PAGE_FAST_PREV = 0;
    public static final int PAGE_PREV = 1;
    public static final int PAGE_CHANGE = 2;
    public static final int PAGE_NEXT = 3;
    public static final int PAGE_FAST_NEXT = 4;

    private List<Post> items;

    private Topic topic;
    public PostAdapter(Topic topic) {
        this.topic = topic;
        this.items = new ArrayList<>();
    }

    public void addItem(Post item) {
        items.add(item);
        notifyDataSetChanged();
    }
    public void setItems(List<Post> items){
        this.items.clear();;
        this.items = items;
        notifyDataSetChanged();
    }

    public Post getItem(int index) {
        return items.get(index);
    }

    public List<Post> getItems() {
        return items;
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_post, parent, false);
            return new PostViewHolder(v);
        }else{
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer, parent, false);
            return new FooterViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if(holder instanceof PostViewHolder){
            ((PostViewHolder) holder).setValue(getItem(position - 1));
        }else if(holder instanceof FooterViewHolder){
            ((FooterViewHolder) holder).setData();

        }

    }

    @Override
    public int getItemCount() {
        return items.size() + 2;
    }


    @Override
    public int getItemViewType(int position) {
        if(isPositionHeader (position)) {
            return TYPE_HEADER;
        } else if(isPositionFooter (position)) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }
    private boolean isPositionHeader (int position) {
        return position == 0;
    }

    private boolean isPositionFooter (int position) {
        return position == items.size () + 1;
    }

    class FooterViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.page_list)
        TextView page_list;
        public FooterViewHolder (View itemView) {
            super (itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setData(){
            page_list.setText(String.format("%d/%d",topic.getCurrentPage(),topic.getTotalPage()));

        }

        @OnClick(R.id.fast_prev)
        void OnClickedFastPrevious(){
            onItemClickListener.OnClickPage(PAGE_FAST_PREV);

        }
        @OnClick(R.id.prev)
        void OnClickedPrevious(){
            onItemClickListener.OnClickPage(PAGE_PREV);

        }
        @OnClick(R.id.next)
        void OnClickedNext(){
            onItemClickListener.OnClickPage(PAGE_NEXT);

        }
        @OnClick(R.id.fast_next)
        void OnClickedFastNext(){
            onItemClickListener.OnClickPage(PAGE_FAST_NEXT);

        }
    }

    public static class PostViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.wvContent)
        TextView webView;
        @BindView(R.id.tvUser)
        TextView tvUser;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvJd)
        TextView tvJd;
        @BindView(R.id.tvPosts)
        TextView tvPosts;
        @BindView(R.id.tvTime)
        TextView tvTime;
        @BindView(R.id.ivAvatar)
        CircleImageView ivAvatar;
        @BindView(R.id.tvNum)
        TextView tvNum;



        public PostViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (onItemClickListener != null) {
//                        onItemClickListener.OnClickItem(v, getLayoutPosition());
//                    }
//                }
//            });
        }

        public void setValue(Post item) {

            PicassoImageGetter picassoImageGetter = new PicassoImageGetter(webView,
                    itemView.getResources(),
                    Glide.with(itemView.getContext()),
                    Picasso.with(itemView.getContext()));

            webView.setText(Html.fromHtml(item.getHtmlContent(), picassoImageGetter, new HtmlTagHandler()));
            webView.setMovementMethod(LinkMovementMethod.getInstance());
            webView.setLinksClickable(true);
            webView.setClickable(false);
            webView.setFocusable(false);
            webView.setLongClickable(false);


            tvUser.setText(item.getUser());
            tvTitle.setText(item.getTitle());
            tvJd.setText(item.getJd());
            tvPosts.setText(item.getPosts());
            tvTime.setText(item.getTimePost());
            tvNum.setText(item.getNumberPost());

            Glide.with(itemView.getContext())
                    .load(item.getUrlAvatar())
                    .placeholder(R.drawable.ic_account_circle_black_48dp)
                    .error(R.drawable.ic_account_circle_black_48dp)
                    .crossFade()
                    .into(ivAvatar);
        }

//        @OnClick({R.id.tvReply, R.id.tvQuote})
//        public void OnClicked(View v){
//            switch (v.getId()) {
//
//                case R.id.tvReply:
//                    onItemClickListener.OnClickReply(v, getLayoutPosition());
//                    break;
//                case R.id.tvQuote:
//                    onItemClickListener.OnClickQuote(v, getLayoutPosition());
//                    break;
//            }
//        }
    }

    private static final String TAG = "PostAdapter";


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public static OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void OnClickItem(View view, int position);
        void OnClickPage(int type);
        void OnClickReply(View view, int position);
        void OnClickQuote(View view, int position);
    }
}
