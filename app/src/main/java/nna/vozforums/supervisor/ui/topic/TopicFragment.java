package nna.vozforums.supervisor.ui.topic;

import javax.inject.Inject;

import nna.vozforums.supervisor.data.model.Box;
import nna.vozforums.supervisor.ui.base.BaseActivity;
import nna.vozforums.supervisor.ui.base.BaseTopicFragment;

/**
 * Created by Ann on 3/13/16.
 */

public class TopicFragment extends BaseTopicFragment implements TopicMvpView {

    @Inject
    TopicPresenter topicPresenter;
    private Box box;


    @Override
    public void inject() {
        ((BaseActivity) getActivity()).activityComponent().inject(this);
    }

    @Override
    public void attachPresenter() {

        topicPresenter.attachView(this);
        setTitle(box.getName());


    }

    @Override
    public void detachPresenter() {
        topicPresenter.detachView();

    }

    @Override
    public void getData() {
        topicPresenter.setBox(box);
        topicPresenter.getData();

    }

    public void setBox(Box box) {
        this.box = box;
    }

}
