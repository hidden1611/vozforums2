package nna.vozforums.supervisor.ui.reply;

import nna.vozforums.supervisor.ui.base.MvpView;

/**
 * Created by Ann on 1/6/17.
 */

public interface ReplyMvpView extends MvpView {

    void updateQuote(String wholeText);

    void showProcessbar(boolean show);

    void showToast(String s);

    void updateReplySuccess();
}
