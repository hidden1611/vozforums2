package nna.vozforums.supervisor.ui.reply;

import android.content.Context;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import javax.inject.Inject;

import nna.vozforums.supervisor.data.DataManager;
import nna.vozforums.supervisor.data.remote.VozService;
import nna.vozforums.supervisor.injection.ApplicationContext;
import nna.vozforums.supervisor.ui.base.BasePresenter;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Ann on 1/6/17.
 */

public class ReplyPresenter extends BasePresenter<ReplyMvpView> {

    public static final int TYPE_REPLY_QUICK = 0;
    public static final int TYPE_REPLY_QUOTE = 1;
    public static final int TYPE_REPLY_EDIT = 2;

    @Inject
    DataManager dataManager;

    private final Context context;
    private Document document;
    int type = TYPE_REPLY_QUICK;

    String idQuote;
    String threadId;

    public void setThreadId(String threadId) {
        this.threadId = threadId;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setIdQuote(String idQuote) {
        this.idQuote = idQuote;
    }

    @Inject
    public ReplyPresenter(@ApplicationContext Context context){

        this.context = context;
    }

    @Override
    public void attachView(ReplyMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public void getMessageReply(){

        String url = null;

        if(type == TYPE_REPLY_QUICK){
            url = VozService.ENDPOINT + "newreply.php?do=postreply&t=" + threadId;
        }else if(type == TYPE_REPLY_QUOTE){
            url = VozService.ENDPOINT + "newreply.php?do=newreply&p=" + idQuote;

        }

        dataManager.fetchData(url)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Document>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        e.printStackTrace();


                    }

                    @Override
                    public void onNext(Document data) {

                        document = data;
                        Element eleQuote = document.select("textarea[name=message]").first();
                        if (eleQuote != null)
                            getMvpView().updateQuote(eleQuote.textNodes().get(0).getWholeText());


                    }
                });
    }

    public void postReply(String message){

        if(message.length() < 20){
            getMvpView().showToast("Chưa đủ 20 chữ!!!");
            return;
        }
        if (isViewAttached()) {
            getMvpView().showProcessbar(true);
        }

        Element eleUrl = document.select("form[action*=newreply.php?do=postreply]").first();
        Element eleSecurity = document.select("input[name*=securitytoken]").first();
        Element elePostHash = document.select("input[name*=posthash]").first();
        Element elePostStartTime = document.select("input[name*=poststarttime]").first();
        Element eleTitle = document.select("input[name=title]").first();
        Element eleP = document.select("input[name=p]").first();
        String mUrl = eleUrl.attr("action");
        mUrl = mUrl.replace("&amp;", "&");
        mUrl = "https://vozforums.com/" + mUrl;

        String token = eleSecurity.attr("value");
        String thread = mUrl.split("=")[2];
        String postHash = elePostHash.attr("value");
        String postStartTime = elePostStartTime.attr("value");
        String p = eleP.attr("value");
        String title = eleTitle.attr("value");

        dataManager.postReply(mUrl, title, message, token, thread, p, "1", postHash, postStartTime  )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Document>() {
                    @Override
                    public void onCompleted() {
                        if (isViewAttached()) {
                            getMvpView().showProcessbar(false);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                        e.printStackTrace();
                        if (isViewAttached()) {
                            getMvpView().showProcessbar(false);
                        }

                    }

                    @Override
                    public void onNext(Document data) {

                        if(isViewAttached()){
                            getMvpView().showProcessbar(false);
                            getMvpView().updateReplySuccess();
                        }

                    }
                });
    }
}
