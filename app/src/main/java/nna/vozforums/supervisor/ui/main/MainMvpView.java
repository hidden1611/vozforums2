package nna.vozforums.supervisor.ui.main;

import nna.vozforums.supervisor.ui.base.MvpView;

/**
 * Created by Ann on 3/12/16.
 */
public interface MainMvpView extends MvpView{


    void showToast(String toast);
    void showProcessbar(boolean show);
}
