package nna.vozforums.supervisor.ui.box;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import nna.vozforums.supervisor.R;
import nna.vozforums.supervisor.data.model.Box;

/**
 * Created by Ann on 3/12/16.
 */
public class BoxAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Box> items;

    public BoxAdapter() {
        this.items = new ArrayList<>();
    }

    public void addItem(Box item) {
        items.add(item);
        notifyDataSetChanged();
    }
    public void setItems(List<Box> items){
        this.items.clear();
        this.items = items;
        notifyDataSetChanged();
    }

    public Box getItem(int index) {
        return items.get(index);
    }

    public List<Box> getItems() {
        return items;
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        Box box = getItem(position);
        if(box !=  null){
            return box.getType();
        }
        return super.getItemViewType(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == Box.NORMAL) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_box, parent, false);
            return new BoxViewHolder(v);
        }else{
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_group_box, parent, false);
            return new GroupBoxViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof BoxViewHolder) {
            ((BoxViewHolder) holder).setValue(getItem(position));
        }else{
            ((GroupBoxViewHolder) holder).setValue(getItem(position));
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public static class BoxViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.tvViewing)
        TextView tvViewing;

        public BoxViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.OnClick(v, getLayoutPosition());
                    }
                }
            });
        }

        public void setValue(Box item) {
            tvName.setText(item.getName());
            tvViewing.setText(item.getViews());

        }
    }

    public static class GroupBoxViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvGroupName)
        TextView tvGroupName;

        public GroupBoxViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setValue(Box item) {
            tvGroupName.setText(item.getName());
        }
    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public static OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void OnClick(View view, int position);
    }
}
