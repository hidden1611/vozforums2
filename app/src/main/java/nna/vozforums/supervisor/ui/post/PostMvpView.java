package nna.vozforums.supervisor.ui.post;

import java.util.List;

import nna.vozforums.supervisor.data.model.Post;
import nna.vozforums.supervisor.ui.base.MvpView;

/**
 * Created by Ann on 3/13/16.
 */
public interface PostMvpView extends MvpView{

    void setPosts(List<Post> posts);

    void showLoading();

    void hideLoading();

    void showError(String s);
}
