package nna.vozforums.supervisor.util;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.text.Html;
import android.widget.TextView;

import com.bumptech.glide.RequestManager;
import com.squareup.picasso.Picasso;

import nna.vozforums.supervisor.data.remote.VozService;
import nna.vozforums.supervisor.ui.main.MainActivity;

public class PicassoImageGetter implements Html.ImageGetter {

    final Resources resources;

    private final RequestManager glide;
    final Picasso picasso;

    final TextView textView;




    public PicassoImageGetter(final TextView textView, final Resources resources, final RequestManager glide, final Picasso picasso) {
        this.textView = textView;
        this.resources = resources;
        this.glide = glide;
        this.picasso = picasso;
    }

    @Override
    public Drawable getDrawable(final String source) {
        final BitmapDrawablePlaceHolder result = new BitmapDrawablePlaceHolder();

//        glide.load(source.startsWith("http") ? source : VozService.ENDPOINT + source)
//                .asGif()
//                .into(new SimpleTarget<GifDrawable>() {
//                    @Override
//                    public void onResourceReady(GifDrawable resource, GlideAnimation<? super GifDrawable> glideAnimation) {
//                        try {
//
//                            int width, height;
//                            if(ViewUtil.dpToPx(resource.getIntrinsicWidth()) > MainActivity.WIDTH){
//                                width = MainActivity.WIDTH;
//                                height = ViewUtil.dpToPx(resource.getIntrinsicHeight()) * width / ViewUtil.dpToPx(resource.getIntrinsicWidth());
//                            }else{
//                                width = ViewUtil.dpToPx(resource.getIntrinsicWidth());
//                                height = ViewUtil.dpToPx(resource.getIntrinsicHeight());
//                            }
//
//                            resource.setBounds(0,
//                                    0,
//                                    width,
//                                    height);
//                            result.setDrawable(resource);
//
//                            result.setBounds(0,
//                                    0,
//                                    width,
//                                    height);
//
//                            if(resource.isAnimated()) {
//                                resource.setLoopCount(GlideDrawable.LOOP_FOREVER);
//                                resource.start();
//                            }
//
//                            textView.setText(textView.getText());
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                });


        new AsyncTask<Void, Void, Bitmap>() {
            @Override
            protected Bitmap doInBackground(final Void... meh) {
                try {
                    return picasso.load(source.startsWith("http") ? source : VozService.ENDPOINT + source)
                            .get();
                } catch (Exception e) {
                    return null;
                }
            }

            @Override
            protected void onPostExecute(final Bitmap bitmap) {
                try {
                    final BitmapDrawable drawable = new BitmapDrawable(resources, bitmap);

                    int width, height;
                    if(ViewUtil.dpToPx(drawable.getIntrinsicWidth()) > MainActivity.WIDTH){
                        width = MainActivity.WIDTH;
                        height = ViewUtil.dpToPx(drawable.getIntrinsicHeight()) * width / ViewUtil.dpToPx(drawable.getIntrinsicWidth());
                    }else{
                        width = ViewUtil.dpToPx(drawable.getIntrinsicWidth());
                        height = ViewUtil.dpToPx(drawable.getIntrinsicHeight());
                    }

                    drawable.setBounds(0,
                            0,
                            width,
                            height);
                    result.setDrawable(drawable);

                    result.setBounds(0,
                            0,
                            width,
                            height);

                    textView.setText(textView.getText());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }.execute((Void) null);

        return result;
    }

    static class BitmapDrawablePlaceHolder extends BitmapDrawable {

        protected Drawable drawable;

        @Override
        public void draw(final Canvas canvas) {
            if (drawable != null) {
                drawable.draw(canvas);
            }
        }

        public void setDrawable(Drawable drawable) {
            this.drawable = drawable;
        }

    }
}