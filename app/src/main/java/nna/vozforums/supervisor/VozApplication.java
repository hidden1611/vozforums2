package nna.vozforums.supervisor;

import android.app.Application;
import android.content.Context;

import com.squareup.otto.Bus;

import javax.inject.Inject;

import nna.vozforums.supervisor.injection.component.ApplicationComponent;
import nna.vozforums.supervisor.injection.component.DaggerApplicationComponent;
import nna.vozforums.supervisor.injection.module.ApplicationModule;
import timber.log.Timber;

/**
 * Created by Ann on 2/12/16.
 */
public class VozApplication extends Application {

    @Inject Bus mEventBus;
    ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        if(BuildConfig.DEBUG){
            Timber.plant(new Timber.DebugTree());
        }
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        applicationComponent.inject(this);
        mEventBus.register(this);

    }

    public static VozApplication get(Context context) {
        return (VozApplication) context.getApplicationContext();
    }

    public ApplicationComponent getComponent() {
        return applicationComponent;
    }

    // Needed to replace the component with a test specific one
    public void setComponent(ApplicationComponent applicationComponent) {
        this.applicationComponent = applicationComponent;
    }
}
